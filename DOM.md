# DOM (Document Object Model)
 When writing web pages and apps, one of the most common things you'll want to do is manipulate the document structure in some way. This is usually done by using the Document Object Model (DOM), a set of APIs for controlling HTML and styling information that makes heavy use of the Document object. In this article we'll look at how to use the DOM in detail, along with some other interesting APIs that can alter your environment in interesting ways.

> **Prerequisites:**  Basic computer literacy, a basic understanding of HTML, CSS, > and JavaScript  including JavaScript objects.
>
>**Objective:** To gain familiarity with the core DOM APIs, and the other APIs commonly associated with DOM and document manipulation.

## What is DOM ?
* The DOM is a W3C (World Wide Web Consortium) standard.
* The DOM defines a standard for accessing documents:

 The W3C Document Object Model (DOM) is a platform and language-neutral interface that allows programs and scripts to dynamically access and update the content, structure, and style of a document.
The W3C DOM standard is separated into 3 different parts:
* Core DOM - standard model for all document types
* XML DOM - standard model for XML documents
* HTML DOM - standard model for HTML documents
## The Document Object Model
* The document currently loaded in each one of your browser tabs is represented by a document object model.
* This is a "tree structure" representation created by the browser that enables the HTML structure to be easily accessed by programming languages for example the browser itself uses it to apply styling and other information to the correct elements as it renders a page, and developers like you can manipulate the DOM with JavaScript after the page has been rendered.
## What is HTML DOM ?
The HTML DOM is a standard object model and programming interface for HTML. It defines:
* The HTML elements as **objects**
* The **properties** of all HTML elements
* The **methods** to access all HTML elements
* The **events** for all HTML elements
## DOM and JavaScript:
* The DOM is not a programming language, but without it, the JavaScript language wouldn't have any model or notion of web pages, HTML documents, XML documents, and their component parts (e.g. elements). 
* Every element in a document is a document as a whole, the head, tables within the document, table headers, text within the table cells is part of the document object model for that document, so that they can all be accessed and manipulated using the DOM and a scripting language like JavaScript.

1. In the beginning, JavaScript and the DOM were mixed, but eventually, they evolved into separate entities.
2. The page content is stored in the DOM and may be accessed and manipulated via JavaScript, so that we may write this approximative equation:
3. Relation between API's, JavaScript and DOM can be described as:
  > **API = DOM + JavaScript**

## How to access DOM:
> NOTE: **You don't have to do anything special to begin using the DOM.**

* When you create a script whether it's inline in a element or included in the web page by means of a script loading instruction you can immediately begin use the API for the document.
* You can also use the window elements to manipulate the document itself or to get at the children of that  document,which are the different elements in the web page. 
## Methods to access DOM using JavaScript:
So, there are in total 5 methods to access DOM using JavaScript. We will be looking some of them in detail.
* getElementById()
* getElementsByClassName()
* getElementsByTagName()
* querySelector()
* querySelectorAll()

### **getElementById():**
The easiest way to access a single element in the DOM is by its unique ID. We can grab an element by ID with the **getElementById()** method of the document object.
> document.getElementById();

In order to be accessed by ID, the HTML element must have an id attribute. We have a div element with an ID of **"demo"**.
> <div id="demo">Access me by ID</div> 

### **getElementsByClassName():**
The class attribute is used to access one or more specific elements in the DOM. We can get all the elements with a given class name with the **getElementsByClassName()** method.

> document.getElementsByClassName();

### **getElementsByTagName():**
A less specific way to access multiple elements on the page would be by its HTML tag name. We access an element by tag with the **getElementsByTagName()** method.

> document.getElementsByTagName();

 

## Testing the DOM API 
* This section provides informationabout every interface that you can use in your own web development. 
* In some cases, the samples are complete HTML pages, with the DOM access in a "script tag" element, the interface (e.g, buttons) necessary to fire up the script in a form, and the HTML elements upon which the DOM operates listed as well. 
* When this is the case, you can cut and paste the example into a new HTML document, save it, and run the example from the browser.

* You can use this test page or create a similar one to test the DOM interfaces you are interested in and see how they work on the browser platform. 
* You can update the contents of the test() function as needed, create more buttons, or add elements as necessary.




References : 
1.  [w3schools.com](https://www.w3schools.com/js/js_htmldom.asp)
2. [MDN web docs](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction)















































































